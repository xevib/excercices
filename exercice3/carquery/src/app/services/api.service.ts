import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  //baseUrl = "https://www.carqueryapi.com/api/0.3/";
  baseUrl = "http://localhost:4200/api/0.3/";

  constructor(
    private http: HttpClient
  ) { }

  getTrims(model:string):Observable<any> {
    const options = {
      params: new HttpParams().set("cmd", "getTrims")
    };

    return this.http.get(this.baseUrl, options);
  }

  getModels(make:string):Observable<any> {
    const options = {
      params: new HttpParams().set('cmd', 'getModels').set('make', make), 
    }

    return this.http.get(this.baseUrl, options);
  }
}
