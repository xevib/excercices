import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Trim } from 'src/app/interfaces/trim';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent {

    @Input("trim") trim:Trim|null = null;
    @Output("closeDetails") closeDetails = new EventEmitter<boolean>();

    close() {
      this.closeDetails.emit(true);
    }
}
