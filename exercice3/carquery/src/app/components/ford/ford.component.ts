import { Component } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { Model } from 'src/app/interfaces/model';

@Component({
  selector: 'app-ford',
  templateUrl: './ford.component.html',
  styleUrls: ['./ford.component.css']
})
export class FordComponent {

    public models:Model[] = [];

    constructor(public api:ApiService){
      this.models = [];
      this.loadModels();
    }

    public loadModels() {
      this.api.getModels('ford').subscribe(
        data => { this.models = data["Models"]}
      );
    }
}
