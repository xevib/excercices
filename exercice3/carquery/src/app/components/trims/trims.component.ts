import { Component, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { Trim } from 'src/app/interfaces/trim';
import { DetailsComponent } from '../details/details.component';

@Component({
  selector: 'app-trims',
  templateUrl: './trims.component.html',
  styleUrls: ['./trims.component.css']
})
export class TrimsComponent {

    

    model:string|null = null;
    trims: Trim[] = [];
    showingDetails:boolean = false;
    @ViewChild('details') details!: DetailsComponent;


    constructor(
      private api:ApiService,
      private activatedRoute: ActivatedRoute,
    ) {
      this.model = this.activatedRoute.snapshot.paramMap.get('model');
      if (this.model!= null){
        this.loadTrims(this.model);
      }
    }

    public loadTrims(model:string){
      this.api.getTrims(model).subscribe(
        data=>{this.trims = data["Trims"]}
      );
    }

    showDetails(trim: Trim) {
      this.details.trim = trim;
      this.showingDetails = true;
    }

    hideDetails() {
      this.showingDetails = false;  
      this.details.trim = null;
    }
}
