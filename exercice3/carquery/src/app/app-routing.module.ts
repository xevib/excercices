import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FordComponent } from './components/ford/ford.component';
import { TrimsComponent } from './components/trims/trims.component';


const routes: Routes = [
  {
    path: "",
    component: FordComponent
  },
  {
    path: "models",
    component: FordComponent
  },{
    path: "trims/:model",
    component: TrimsComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
