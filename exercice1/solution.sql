SELECT
	regions.name, -- Region name
	count(sales.id) / count(DISTINCT(employees.id)) as avg_sales_employee , -- (Average of number of sales per employee)
--	count(sales.amount) / count(DISTINCT(employees.id)) as avg_amount_employee , -- (Average of amount sales per employee)
	abs(avg(sales.amount) - max(sales.amount)) as difference,
	avg(sales.amount)
FROM regions
LEFT JOIN states ON states.region_id  = regions.id 
LEFT JOIN employees ON employees.state_id  = states.id
LEFT JOIN sales ON sales.employee_id = employees.id
GROUP BY regions.id