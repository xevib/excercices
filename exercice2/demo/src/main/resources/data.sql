INSERT INTO public.employees (id,"name",state_id) VALUES
   (1,'Treballador 1',1),
   (2,'Treballador 2',1),
   (3,'Treballador 3',2),
   (4,'Treballador 4',2),
   (5,'Treballador 5',2);
INSERT INTO public.regions ("name",id) VALUES
   ('Girona',1),
   ('Barcelona',2);
INSERT INTO public.sales (id,amount,employee_id) VALUES
   (1,100,1),
   (2,100,1),
   (3,200,2),
   (4,300,2);
INSERT INTO public.states ("name",id,region_id) VALUES
   ('State 1',1,1),
   ('State 2',2,2);