package com.example.demo.api;

import java.util.List;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


public interface EmployeeApi {

	@RequestMapping(value = "/employees", method = RequestMethod.GET)
	ResponseEntity<List<String>> comarquesGet();

	
}
