package com.example.demo.controllers;

import java.util.List;

//import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.example.demo.api.EmployeeApi;
import com.example.demo.services.EmployeeService;


@CrossOrigin("*")
@Controller
public class EmployeeApiController implements EmployeeApi {

	@Autowired
	private EmployeeService employeeService;


	public ResponseEntity<List<String>> comarquesGet(){
		return new ResponseEntity<List<String>>(employeeService.getEmployees(), HttpStatus.OK);
	}

	


}
